import socket
import thread
import time
import pymysql
import sqlite3

def check_t_h(vals,con, c):
    data=[float(i) for i in vals.split()]
    print data
    if data:
        cur = con.cursor()
        cur.execute('update Farm_table set temp = %f where id = %d;' % (data[1],1) )
        cur.execute('update Farm_table set hum = %f where id = %d;' % (data[0],1))
        con.commit()
        cur.execute('select override from Farm_table where id = 1;')
        override = cur.fetchone()[0]
        if data[0] < 60 and data[1] >=25 and data[1]<35 and not(override) :
            c.send('open')
            cur.execute("update Farm_table set valve = 'open' where id = %d;"% 1)
            con.commit()
            print "opening valve"
        elif override :
            c.send('close')
            cur.execute("update Farm_table set valve = 'close' where id = %d;"% 1)
            con.commit()
            print "closing valve"
        else:
            pass

def main(s):
    print 'binding the socket to the port'
    s.bind(('0.0.0.0', 3000))

    print ' Now wait for client connection.'
    s.listen(5)
    con = pymysql.connect('localhost','root','abhinavthomas','irrigation')
    cur = con.cursor()

    while True:
        try:
            print 'signal sending to client '
            data = c.recv(64)
            if not data:
                raise Exception, 'No data'
        except KeyboardInterrupt:
                exit()
        except :
            print "Sockets not available. Waiting for reconnection"
            try:
                cur.execute("update Farm_table set pi_stat = 'offline' where id = %d;" % 1)
                con.commit()
                c,addr = s.accept()            
            except:
                pass
        else:
            cur.execute("update Farm_table set pi_stat = 'online' where id = %d;" % 1)
            con.commit()
            print data 
            thread.start_new_thread( check_t_h, (data,con, c,) )
            time.sleep(5)

if __name__ == '__main__':
    
    #creating a server socket to accept connections
    s = socket.socket()
    print 'socket created entering main'
    try:
        main(s)
    except KeyboardInterrupt:
        exit()
    @atexit.register
    def goodbye():
        print "Exiting....."
        s.close()
