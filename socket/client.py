import socket
import RPi.GPIO as GPIO
GPIO.setmode(GPIO.BCM)
GPIO.setup(22,GPIO.OUT)
import time
import thread
import signal
from contextlib import contextmanager
import Adafruit_DHT as dht
flag = False
class TimeoutException(Exception): pass

@contextmanager
def time_limit(seconds):
    def signal_handler(signum, frame):
        raise TimeoutException, "Timed out!"
    signal.signal(signal.SIGALRM, signal_handler)
    signal.alarm(seconds)
    try:
        yield
    finally:
        signal.alarm(0)

def send_to_server(s):
    print "in send_to_server"
    while(True):
        print "inside s_t_s loop"
        data=dht.read_retry(dht.DHT11,4)
        val = str(data[0])+' '+str(data[1])
        print val
        try:
            print "before sending to server"
            s.send(val)
        except:
            # global flag 
            # flag = False
            print "exception in send_to_server"
            break
        else:
            print val+' sent'
        time.sleep(5)

def rec_from_server(s):
    fp = open ('a.txt','w')
    fp.write("in rec_from_server")
    print "in rec_from_server"
    while(True):
        try:
            print "inside while"
            # with time_limit(10):
            fp.write("with time lim")
            print "with time lim"
            data = s.recv(64)
            fp.write("recieved")
            print data
        except:
            fp.write("rec_from_ser error")
            print "rec_from_ser error"
            # break
        else:
            if 'open' in data:
                GPIO.output(22,1)
                fp.write("valve opened")
                print "valve opened"
                time.sleep(5)
                fp.write("valve closed")
                print "valve closed"
                GPIO.output(22,0)
    fp.close()

def main():
    while(True):
        s=socket.socket()
        try:
            s.connect(('192.168.43.164',3000))
            print "connected"
            thread.start_new_thread(rec_from_server,(s,))
            print "new thread started"
            send_to_server(s)
            print "before closing"
            s.close()
        except:
            s.close()
            print "Connection failed. Reconnecting"
            continue
        # if flag == True:
        #     pass
if __name__=='__main__':
    main()
    @atexit.register
    def goodbye():
        print "Exiting....."
        s.close()
