from __future__ import unicode_literals

from django.db import models

# Create your models here.

class Farm(models.Model):

   farm_id = models.CharField(max_length = 50)
   pi_stat = models.CharField(max_length = 50)
   temp = models.DecimalField(max_digits=10, decimal_places=5)
   hum = models.DecimalField(max_digits=10, decimal_places=5)
   override = models.IntegerField()
   valve = models.CharField(max_length = 50)
   
   class Meta:
      db_table = "Farm_table"